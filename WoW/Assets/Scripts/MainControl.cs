﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainControl : MonoBehaviour {
    public float MovingMass = 100;
    public InputField movingMassField;

    public static GameObject startNodeObj, endNodeObj;

    public GameObject startButtonObj;

    public static bool SpeedFocus = false;
    public Toggle focusToggle;

    public void FocusChanged ()
    {
        SpeedFocus = focusToggle.isOn;
    }

    public void MovingMassChanged () {
        float.TryParse(movingMassField.text, out MovingMass);
    }

    public void StartButtonPressed () {
        //Select a starting vertex and a destination vertex.
        if (MouseCtrl.selectionStatus == MouseCtrl.SelectionStatus.CREATEGRAPH &&
            !MouseCtrl.canvasFlag)
        {
            MouseCtrl.selectionStatus = MouseCtrl.SelectionStatus.SELECTINGSTART;
            Destroy(startButtonObj);
        } 
    }

    void Update() {
        
    }
}
