﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Edge
{
    public float inputWeight;
    public float realWeight;
    public EdgeEdit.ButtonTypeState pathType;
    public Node target;
    public GameObject lineRendererObj;
}

public class Node : MonoBehaviour {
    public List<Edge> outgoingEdges = new List<Edge>();
    void Start () {
        transform.position = new Vector3(transform.position.x,
                                         transform.position.y,
                                         -5);
    }
}
