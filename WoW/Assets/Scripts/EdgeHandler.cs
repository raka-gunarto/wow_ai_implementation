﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeHandler : MonoBehaviour {
    LineRenderer line;
    CapsuleCollider capsule;
    public GameObject n1, n2;
    public Edge e1, e2;

    public void SetEdgeWeight (float nw)
    {
        print("set edge weight called " + nw);
        e1.realWeight = e2.realWeight = nw;      
    }

    void Start() {
        line = GetComponent<LineRenderer>();
        capsule = gameObject.AddComponent<CapsuleCollider>();
        capsule.radius = line.startWidth;
        capsule.center = Vector3.zero;
        capsule.direction = 2;
    }

    void Update() {
        capsule.transform.position = line.GetPosition(0) + (line.GetPosition(1) - line.GetPosition(0)) / 2;
        capsule.transform.LookAt(line.GetPosition(0));
        capsule.height = (line.GetPosition(1) - line.GetPosition(0)).magnitude;
    }
}
