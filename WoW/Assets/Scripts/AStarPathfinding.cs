﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

using UnityEngine;

public struct NodeWithDistance
{
    public Node n;
    public float distance;
    public Edge? originEdge;
}

public class AStarPathfinding : MonoBehaviour {
    public EdgeManager edgeManager;
    public Material[] materials = new Material[3];
    Dictionary<string, NodeWithDistance> parentDict = new Dictionary<string, NodeWithDistance>();
    public List<NodeWithDistance> nodesToCheck = new List<NodeWithDistance>();
    public bool finished = false;
    public bool found = false;
    public bool started = false;
    public enum MaterialTypes
    {
        CHECKING,
        CHECKED,
        OPTIMUM
    }
    public void StartAStar ()
    {
        print("A star pathfinding function called");
        GameObject startNode = MainControl.startNodeObj;
        GameObject destNode = MainControl.endNodeObj;
        List<float> edgeWeights = edgeManager.edgeWeights;
        List<string> closedNodeNames = new List<string>();

         NodeWithDistance originFromEnd;

        if (!started)
        {
            NodeWithDistance nwd;

            nwd.n = startNode.GetComponent<Node>();
            nwd.distance = 0;
            nwd.originEdge = null;

            nodesToCheck.Add(nwd);
            started = true;
            originFromEnd = nwd;
        }
        
       

        if(nodesToCheck.Count > 0)
        {
            nodesToCheck = nodesToCheck.OrderBy(o => o.distance).ToList();
            NodeWithDistance curNwd = nodesToCheck[0];
            print("t: " + curNwd.n.name);
            if(curNwd.originEdge != null)
            {
                Edge oriEdge = curNwd.originEdge.GetValueOrDefault();
                oriEdge.lineRendererObj.GetComponent<LineRenderer>().material = materials[1];
            }
            foreach(Edge e in curNwd.n.outgoingEdges){
                Node target = e.target;
                print(target.name);
                if (target == curNwd.n)
                {
                    continue;
                }
                
                NodeWithDistance targetNwd;
                targetNwd.n = target;
                targetNwd.distance = e.realWeight + curNwd.distance;
                targetNwd.originEdge = e;
                parentDict[target.name] = curNwd;

                if (target.name == destNode.name)
                {
                    print("found");
                    e.lineRendererObj.GetComponent<LineRenderer>().material = materials[2];
                    found = true;
                    originFromEnd = targetNwd;
                    break;
                }

                nodesToCheck.Add(targetNwd);
                e.lineRendererObj.GetComponent<LineRenderer>().material = materials[0];
            }
            
            closedNodeNames.Add(curNwd.n.name);
            nodesToCheck.RemoveAt(0);
        }


        //Backtrack
        if (found)
        {
            finished = true;
            print("Backtracking");
            NodeWithDistance curNwd = parentDict[originFromEnd.n.name];
            while (true)
            {
                print("Backtracking");
                curNwd.originEdge.GetValueOrDefault().lineRendererObj.GetComponent<LineRenderer>().material = materials[2];
                try
                {
                    curNwd = parentDict[curNwd.n.name];
                }
                catch(Exception e){  // Reached origin. No more parent
                    break;
                }
            }
            curNwd.originEdge.GetValueOrDefault().lineRendererObj.GetComponent<LineRenderer>().material = materials[2];
            finished = true;
        }

    }
}
