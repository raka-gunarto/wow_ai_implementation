﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;
using UnityEngine;

public class EdgeEdit : MonoBehaviour {
    private ButtonTypeState state = ButtonTypeState.SEA;

    public enum ButtonTypeState
    {
        AIR,
        SEA,
        TRAIN
    }

    public Material[] materials = new Material[3];

    public InputField weightInputField;

    public void WeightChanged() {
        //Convert the text in the field into numeric data.
        string weightInput = weightInputField.text;
        float weight = 0.0f;
        float.TryParse(weightInput, out weight);
        EdgeHandler eh = (MouseCtrl.activeEdge.GetComponent("EdgeHandler") as EdgeHandler);
        eh.e1.inputWeight = eh.e2.inputWeight = weight;
        eh.e1.realWeight = eh.e2.realWeight = EdgeManager.CalculateTrueWeight(weight, state);
    }

    public void TypeBtnOnClick()
    {
        GameObject btnGO = GameObject.Find("Button");

        Text text = btnGO.transform.GetChild(0).GetComponent<Text>();
        state = (ButtonTypeState)((int)(state + 1) % 3);

        GameObject activeEdge = MouseCtrl.activeEdge;

        float cw = (activeEdge.GetComponent("EdgeHandler") as EdgeHandler).e1.inputWeight;
        (activeEdge.GetComponent("EdgeHandler") as EdgeHandler).SetEdgeWeight(EdgeManager.CalculateTrueWeight(cw, state));

        switch (state)
        {
            case ButtonTypeState.AIR:
                activeEdge.GetComponent<Renderer>().material = materials[(int)state];
                text.text = "Air";
                break;
            case ButtonTypeState.SEA:
                activeEdge.GetComponent<Renderer>().material = materials[(int)state];
                text.text = "Sea";
                break;
            case ButtonTypeState.TRAIN:
                activeEdge.GetComponent<Renderer>().material = materials[(int)state];
                text.text = "Train";
                break;
        }
    }
	void Start () {
        gameObject.name = "EdgeEdit";
        float weight = 0.0f;
        EdgeHandler eh = (MouseCtrl.activeEdge.GetComponent("EdgeHandler") as EdgeHandler);
        weight = eh.e1.inputWeight;
        weightInputField.text = weight.ToString();
    }
}
