﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public struct EdgeToDel
{
    public Edge e;
    public Node n;
}

public class MouseCtrl : MonoBehaviour
{
    [SerializeField] private GameObject nodePrefab;
    [SerializeField] private EdgeManager edgeManager;
    [SerializeField] private GameObject edgeEditPrefab;
    [SerializeField] private Material selectedNodeMat;
    [SerializeField] private Material destinationNodeMat;
    [SerializeField] private AStarPathfinding pathfinder;

    //Are we currently creating an edge from any object?
    private bool creatingEdge = false;
    private GameObject extendedNode;
    public static bool canvasFlag = false;

    float time = 1.0f;

    GameObject edgeEdit = null;

    private int nodeID = 0;

    public static GameObject activeEdge = null;
    bool pathfinding = false;
    public enum SelectionStatus
    {
        CREATEGRAPH,
        SELECTINGSTART,
        SELECTINGDEST
    };
    public static SelectionStatus selectionStatus;

    void updateAStar()
    {
        pathfinder.StartAStar();
    }

    void Update()
    {
        if (pathfinding)
        {
            if (!pathfinder.finished)
            {
                if (time >= 0)
                {
                    time -= Time.deltaTime;
                }
                else
                {
                    updateAStar();
                    time = 1.0f;
                }
            }
        }
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            //Raycast forward and see if we have clicked on
            //an already-existing node, or the background (which
            //will create a new node).
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x,
                                                               Input.mousePosition.y,
                                                               0));
            if (Physics.Raycast(ray, out hit, 1000))
            {
                switch (hit.transform.gameObject.tag)
                {
                    case "Node":
                        switch (selectionStatus)
                        {
                            case SelectionStatus.CREATEGRAPH:
                                if (canvasFlag)
                                {
                                    canvasFlag = false;
                                    Destroy(edgeEdit);
                                    edgeEdit = null;
                                    break;
                                }
                                if (!creatingEdge)
                                {
                                    //Create an edge from this node.
                                    creatingEdge = true;
                                    extendedNode = hit.transform.gameObject;
                                }
                                else
                                {
                                    //Create an edge to this node.
                                    if (hit.transform.gameObject != extendedNode)
                                    {
                                        creatingEdge = false;
                                        edgeManager.CreateEdge(extendedNode.GetComponent("Node") as Node,
                                            hit.transform.gameObject.GetComponent("Node") as Node,
                                            1);
                                    }
                                }
                                break;
                            case SelectionStatus.SELECTINGSTART:
                                MainControl.startNodeObj = hit.transform.gameObject;
                                hit.transform.gameObject.GetComponent<Renderer>().material = selectedNodeMat;
                                selectionStatus = SelectionStatus.SELECTINGDEST;
                                break;
                            case SelectionStatus.SELECTINGDEST:
                                MainControl.endNodeObj = hit.transform.gameObject;
                                hit.transform.gameObject.GetComponent<Renderer>().material = destinationNodeMat;
                                pathfinding = true;
                                break;
                        }
                        break;
                    case "Background":
                        if (selectionStatus == SelectionStatus.CREATEGRAPH)
                        {
                            if (canvasFlag)
                            {
                                canvasFlag = false;
                                Destroy(edgeEdit);
                                edgeEdit = null;
                                break;
                            }
                            //Create a new node
                            GameObject newNode = Instantiate(nodePrefab,
                                                             hit.point,
                                                             Quaternion.identity) as GameObject;
                            newNode.GetComponent<Node>().name = "Node" + nodeID.ToString();
                            nodeID++;
                            edgeManager.nodes.Add(newNode.GetComponent("Node") as Node);
                        }
                        break;
                    case "Edge":
                        if (selectionStatus == SelectionStatus.CREATEGRAPH)
                        {
                            //Edit the weight and type of an edge
                            //Remove the current edge edit UI if one exists
                            GameObject co = GameObject.Find("Canvas");
                            for (int i = 0; i < co.transform.childCount; i++)
                            {
                                Transform cc = co.transform.GetChild(i);
                                if (cc.gameObject.name == "EdgeEdit")
                                {
                                    Destroy(cc.gameObject);
                                }
                            }
                            //Create a new edge edit menu for the current selected edge
                            edgeEdit = Instantiate(edgeEditPrefab,
                                                              edgeEditPrefab.transform.position,
                                                              Quaternion.identity) as GameObject;
                            edgeEdit.transform.SetParent(co.transform, false);
                            canvasFlag = true;
                            activeEdge = hit.transform.gameObject;
                        }
                        break;
                }
            }
        }
        /*if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x,
                                                               Input.mousePosition.y,
                                                               0));
            if (Physics.Raycast(ray, out hit, 1000))
            {
                switch (hit.transform.gameObject.tag)
                {
                    case "Node":
                        Node n = hit.transform.gameObject.GetComponent("Node") as Node;
                        List<EdgeToDel> targetEdgesToDel = new List<EdgeToDel>();

                        foreach (Edge e in n.outgoingEdges)
                        {
                            foreach(Edge e2 in e.target.outgoingEdges)
                            {
                                if(e2.target == n)
                                {
                                    EdgeToDel eX;
                                    eX.e = e2;
                                    eX.n = e.target;
                                    targetEdgesToDel.Add(eX);
                                }
                            }
                            Destroy(e.lineRendererObj);
                        }

                        foreach(EdgeToDel eX in targetEdgesToDel)
                        {
                            eX.n.outgoingEdges.Remove(eX.e);
                        }

                        edgeManager.nodes.Remove(hit.transform.gameObject.GetComponent("Node") as Node);
                        Destroy(hit.transform.gameObject);
                        break;
                }
            }
        }*/
    }
}

