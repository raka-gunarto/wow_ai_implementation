﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeManager : MonoBehaviour {
    public List<float> edgeWeights = new List<float>();
    public List<Node> nodes;
    [SerializeField] private GameObject lineRend;

    //The following function converts distance to effective cost,
    //and the A* pathfinding function uses the effective costs as weights
    public static float CalculateTrueWeight (float givenWeight, EdgeEdit.ButtonTypeState pathType)
    {
        switch (pathType)
        {
            case EdgeEdit.ButtonTypeState.AIR:
                if (MainControl.SpeedFocus)
                {
                    print("speed focus air go " + givenWeight + " to " + (givenWeight / 2.0f));
                    return (givenWeight / 2.0f);
                } else
                {
                    print("speed focus air go " + givenWeight + " to " + (givenWeight * 3.0f));
                    return (givenWeight * 3.0f);
                }
                break;
            case EdgeEdit.ButtonTypeState.SEA:
                if (MainControl.SpeedFocus)
                {
                    return (givenWeight * 3.0f);
                }
                else
                {
                    return (givenWeight / 2.0f);
                }
                break;
            case EdgeEdit.ButtonTypeState.TRAIN:
                if (MainControl.SpeedFocus)
                {
                    return (givenWeight * 2.0f);
                }
                else
                {
                    return (givenWeight);
                }
                break;
        }
        return 0.0f;    //Never happens;
    }

    public void CreateEdge (Node n1, Node n2, float w) {
        //Draw a line between the two nodes.
        GameObject lr = Instantiate(lineRend,
                                    Vector3.zero,
                                    Quaternion.identity) as GameObject;
        lr.GetComponent<LineRenderer>().SetPosition(0, n1.gameObject.transform.position);
        lr.GetComponent<LineRenderer>().SetPosition(1, n2.gameObject.transform.position);
        Edge e1;
        e1.target = n2;
        e1.inputWeight = w;
        e1.pathType = EdgeEdit.ButtonTypeState.SEA;
        e1.realWeight = CalculateTrueWeight(w, e1.pathType);
        e1.lineRendererObj = lr;
        n1.outgoingEdges.Add(e1);
        Edge e2;
        e2.target = n1;
        e2.inputWeight = w;
        e2.pathType = EdgeEdit.ButtonTypeState.SEA;
        e2.realWeight = CalculateTrueWeight(w, e2.pathType);
        e2.lineRendererObj = lr;
        n2.outgoingEdges.Add(e1);
        (lr.GetComponent("EdgeHandler") as EdgeHandler).e1 = e1;
        (lr.GetComponent("EdgeHandler") as EdgeHandler).e2 = e2;

    }
}
